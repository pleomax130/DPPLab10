from setuptools import setup, find_packages

setup(
    name='weather-calculator',
    version='0.1',
    packages=find_packages(exclude=['venv*']),
    license='MIT',
    description='Calculator weather',
    long_description=open('README.md').read(),
    install_requires=['glob', 'csv'],
    url='https://gitlab.com/pleomax130/DPPLab10',
    author='Przemek Praca',
    author_email='przemyslaw.praca96@gmail.com'
)