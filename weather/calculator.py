import csv
import glob
from cmath import exp, cos

y = 0.0065
M = 0.0289644
R = 8.31432
# StationId = "BART"
# path = "C:/Users/Przemek/PycharmProjects/Lab7/weather/*.csv"


def eSatFunc(t):
    return 6.112 * exp((17.67 * (t - 273.15)) / ((t - 273.15) + 243.5))


def ztdFunc(p, t, e):
    return 0.002277 * (p + ((1255 / t) + 0.05) * e)


def eFunc(rh, eSat):
    return (rh * eSat) / 100


def pFunc(pi, ti, h, hi, g):
    return pi * pow((ti - y * (h - hi)) / ti, (g * M) / (R * y))


def gFunc(h, hi, o):
    return 9.8063 * (1 - (pow(10, -7)) * ((hi + h) / 2) *
                     (1 - 0.0026373 * cos(2 * o) + 5.9 *
                      pow(10, -6) * pow(cos(2 * o), 2)))


def weightFunc(x, y):
    return 1 / ((y + 2) - (x * 6))


def calculate_weather(path, StationId):
    j = 0
    sums = [0] * 66
    weights = [0] * 66
    stations = []
    for file in glob.glob(path):
        with open(file, newline='') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            ztds = []
            i = 0 + j * 6
            weight = 0
            for row in csvreader:
                str = '. '.join(row)
                list = str.split(',', str.__sizeof__())
                if list[2] == StationId:
                    interpo = list[3]
                    lat = float(list[4])
                    lon = float(list[5])
                    x = float(list[6])
                    y = float(list[7])
                    h = float(list[8])
                    hWRF = float(list[9])
                    t = float(list[10])
                    rh = float(list[11])
                    p = float(list[12])
                    esat = eSatFunc(t)
                    e = eFunc(rh, esat)
                    if list[3] == "2m":
                        g = gFunc(h, hWRF, lat)
                        P = pFunc(p, t, h, hWRF, g)
                        ztd = ztdFunc(P, t, e).real
                        sums[i] += ztd * (1 / (weight + 2))
                        weights[i] += 1 / (weight + 2)
                        # di = {"id": i, "ztd": ztd * (1 / (weight + 2))}
                        # ztds.append(di)
                        i += 1
                    elif list[3] == "interp":
                        ztd = ztdFunc(p, t, e).real
                        sums[i] += ztd * (1 / (weight + 2))
                        weights[i] += 1 / (weight + 2)
                        # di = {"id": i, "ztd": ztd * (1 / (weight + 2))}
                        # ztds.append(di)
                        i += 1
                    weight += 1
            stations.append(ztds)

        j += 1
    return sums
# for x in range(0, 65):
#     sums[x] /= weights[x]
# print(sums)
